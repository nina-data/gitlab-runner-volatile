FROM gitlab/gitlab-runner:latest

COPY volatile.sh /
ENTRYPOINT
CMD ["/volatile.sh"]
