#!/bin/sh

head -n-1 /entrypoint > prepare.sh
./prepare.sh
gitlab-runner register \
              --non-interactive \
              --url "${URL}" \
              --registration-token "${REGISTRATION_TOKEN}" \
              --executor "docker" \
              --docker-image alpine:3 \
              --description "docker-runner" \
              --tag-list "${TAG_LIST}" \
              --run-untagged \
              --locked="false" \
              --docker-privileged
trap "exec gitlab-runner unregister --url ${URL} --all-runners" TERM
gitlab-runner run --user=gitlab-runner --working-directory=/home/gitlab-runner &
while true; do :; done
